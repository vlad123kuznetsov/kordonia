﻿using Assets.Scripts.Commands;
using Assets.Scripts.Services;
using Kordonia.Data;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

public class InitializerContext : MVCSContext
{
    public InitializerContext(MonoBehaviour view) : base(view)
    {
    }

    public InitializerContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
    {
    }

    protected override void mapBindings()
    {
        injectionBinder.Bind<UserProfile>().To<UserProfile>().CrossContext();
        injectionBinder.Bind<IUserService>().To<LocalUserService>().CrossContext();
        injectionBinder.Bind<IFieldLoader>().To<PngFieldLoader>().CrossContext();

        commandBinder.Bind(ContextEvent.START)
            .To<LoginCommand>()
            .To<CreateMultiplaterConnector>()
            .To<LoadBattleCommand>().InSequence();
    }
}