﻿using Assets.Scripts.Data;
using strange.extensions.mediation.impl;

namespace Assets.Scripts
{
    public class HideableViewMediator : EventMediator
    {
        [Inject]
        public HidebleViewsManager view { get; set; }

        public override void OnRegister()
        {
            dispatcher.AddListener(GameEvents.ShowView, view.Show);
        }

        public override void OnRemove()
        {
            dispatcher.RemoveListener(GameEvents.ShowView, view.Show);
        }
    }
}