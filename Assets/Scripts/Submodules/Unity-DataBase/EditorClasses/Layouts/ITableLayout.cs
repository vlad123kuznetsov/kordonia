﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace EJaw.EditorClasses.UnityDatabase.Layouts
{
    public interface ITableLayout
    {
        /// <summary>
        /// Show caption with default size
        /// </summary>
        void Caption(string caption);

        /// <summary>
        /// Show caption with manual size
        /// </summary>
        void Caption(string caption, float width, float height);

        /// <summary>
        /// Show clickable caption with default size
        /// </summary>
        void ClickableCaption(string caption, Action onClick);

        /// <summary>
        /// Show clickable caption with manual size
        /// </summary>
        void ClickableCaption(string caption, Action onClick, float width, float height);

        /// <summary>
        /// Add string field with default size
        /// </summary>
        string Field(string val);

        /// <summary>
        /// Add string field with manual size
        /// </summary>
        string Field(string val, float width, float height);

        /// <summary>
        /// Add int field with default size
        /// </summary>
        int Field(int value);

        /// <summary>
        /// Add int field with manual size
        /// </summary>
        int Field(int value, float width, float height);

        /// <summary>
        /// Add float field with default size
        /// </summary>
        float Field(float value);

        /// <summary>
        /// Add float field with manual size
        /// </summary>
        float Field(float value, float width, float height);

        /// <summary>
        /// Add toggle field with default size
        /// </summary>
        bool Field(bool value);

        /// <summary>
        /// Add toggle field with manual size
        /// </summary>
        bool Field(bool value, float width, float height);

        /// <summary>
        /// Add popup field with default size
        /// </summary>
        string Field(string current, IEnumerable<string> data);

        /// <summary>
        /// Add popup field with manual size
        /// </summary>
        string Field(string current, IEnumerable<string> data, float width, float height);

        /// <summary>
        /// Add popup field with default size
        /// </summary>
        int Field(int value, IEnumerable<int> ids, IEnumerable<string> names);

        /// <summary>
        /// Add popup field with manual size
        /// </summary>
        int Field(int value, IEnumerable<int> ids, IEnumerable<string> names, float width, float height);

        /// <summary>
        /// Add object field with default size
        /// </summary>
        TUnityObject Field<TUnityObject>(TUnityObject value)
            where TUnityObject : UnityEngine.Object;

        /// <summary>
        /// Add object field with manual size
        /// </summary>
        TUnityObject Field<TUnityObject>(TUnityObject value, float width, float height)
            where TUnityObject : UnityEngine.Object;

        /// <summary>
        /// Add vector3 field with default size
        /// </summary>
        Vector3 Field(Vector3 value);

        /// <summary>
        /// Add vector4 field with default size
        /// </summary>
        Vector4 Field(Vector4 value);

        /// <summary>
        /// Add vector3 field with manual size
        /// </summary>
        Vector3 Field(Vector3 value, float width, float height);

        /// <summary>
        /// Add vector4 field with manual size
        /// </summary>
        Vector4 Field(Vector4 value, float width, float height);

        /// <summary>
        /// Add vector2 field with default size
        /// </summary>
        Vector2 Field(Vector2 value);

        /// <summary>
        /// Add layerMask field 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        LayerMask MaskField(int value);

        /// <summary>
        /// Add vector2 field with manual size
        /// </summary>
        Vector2 Field(Vector2 value, float width, float height);

        /// <summary>
        /// Add enum field with default size
        /// </summary>
        System.Enum EnumField(System.Enum value);

        /// <summary>
        /// Add enum field with manual size
        /// </summary>
        System.Enum EnumField(System.Enum value, float width, float height);

        /// <summary>
        /// Start to draw array
        /// </summary>
        void BeginDrawArray();

        /// <summary>
        /// End drawing of array
        /// </summary>
        void EndDrawArray();

        /// <summary>
        /// Start drawing in one round
        /// </summary>
        void BeginDrawIteration();

        /// <summary>
        /// End drawing in one round
        /// </summary>
        void EndDrawIteration();

        /// <summary>
        /// Drawing one string element in array with default size
        /// </summary>
        string DrawArrayElement(string value);

        /// <summary>
        /// Drawing one string element in array with manual size
        /// </summary>
        string DrawArrayElement(string value, float width, float height);

        /// <summary>
        /// Drawing one int element in array with default size
        /// </summary>
        int DrawArrayElement(int value);

        /// <summary>
        /// Drawing one int element in array with manual size
        /// </summary>
        int DrawArrayElement(int value, float width, float height);

        /// <summary>
        /// Drawing one float element in array with default size
        /// </summary>
        float DrawArrayElement(float value);

        /// <summary>
        /// Drawing one float element in array with manual size
        /// </summary>
        float DrawArrayElement(float value, float width, float height);

        /// <summary>
        /// Drawing one bool element in array with default size
        /// </summary>
        bool DrawArrayElement(bool value);

        /// <summary>
        /// Drawing one bool element in array with manual size
        /// </summary>
        bool DrawArrayElement(bool value, float width, float height);

        /// <summary>
        /// Drawing popup string element in array with default size
        /// </summary>
        string DrawArrayElement(string current, IEnumerable<string> data);

        /// <summary>
        /// Drawing popup string element in array with manual size
        /// </summary>
        string DrawArrayElement(string current, IEnumerable<string> data, float width, float height);

        /// <summary>
        /// Drawing popup int element in array with default size
        /// </summary>
        int DrawArrayElement(int value, IEnumerable<int> ids, IEnumerable<string> names);

        /// <summary>
        /// Drawing popup int element in array with manual size
        /// </summary>
        int DrawArrayElement(int value, IEnumerable<int> ids, IEnumerable<string> names, float width,
            float height);

        /// <summary>
        /// Drawing object element in array with default size
        /// </summary>
        TUnityObject DrawArrayElement<TUnityObject>(TUnityObject value)
            where TUnityObject : UnityEngine.Object;

        /// <summary>
        /// Drawing object element in array with manual size
        /// </summary>
        TUnityObject DrawArrayElement<TUnityObject>(TUnityObject value, float width, float height)
            where TUnityObject : UnityEngine.Object;

        /// <summary>
        /// Drawing one vector3 element in array with default size
        /// </summary>
        Vector3 DrawArrayElement(Vector3 value);

        /// <summary>
        /// Drawing one vector3 element in array with manual size
        /// </summary>
        Vector3 DrawArrayElement(Vector3 value, float width, float height);

        /// <summary>
        /// Drawing one vector2 element in array with default size
        /// </summary>
        Vector2 DrawArrayElement(Vector2 value);

        /// <summary>
        /// Drawing one vector2 element in array with manual size
        /// </summary>
        Vector2 DrawArrayElement(Vector2 value, float width, float height);

        /// <summary>
        /// Drawing one enum element in array with default size
        /// </summary>
        System.Enum DrawEnumArrayElement(System.Enum value);

        /// <summary>
        /// Drawing one enum element in array with manual size
        /// </summary>
        System.Enum DrawEnumArrayElement(System.Enum value, float width, float height);

        /// <summary>
        /// Create new button in tables layout with default size
        /// </summary>
        bool Button(string buttonName, Color buttonColor, bool buttonEnabled = true);

        /// <summary>
        /// Create new button in tables layout with manual size
        /// </summary>
        bool Button(string buttonName, Color buttonColor, bool buttonEnabled, float width, float height);

        /// <summary>
        /// Cach exception and log it
        /// </summary>
        void ExceptionCatch(string message, MessageType type = MessageType.Warning);
    }
}

#endif
