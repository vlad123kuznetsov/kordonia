﻿#if UNITY_EDITOR
using EJaw.UnityDatabase.Records;
using EJaw.UnityDatabase.Tables;

namespace EJaw.EditorClasses.UnityDatabase.Layouts
{
    public interface IWindowLayout
    {
        /// <summary>
        /// Drawing tables based on aligment
        /// </summary>
        void DrawTable<TRecord>(IEditorTable<TRecord> table) where TRecord : IRecord, IEditorRecord;

        /// <summary>
        /// Drawing records based on aligment
        /// </summary>
        void DrawRecord<TTable, TRecord>(TTable table)
            where TTable : ScriptableObjectTable, ITable<TRecord>, IEditorTable<TRecord>
            where TRecord : IRecord, IEditorRecord, new();
    }
}

#endif
