﻿#if UNITY_EDITOR

namespace EJaw.EditorClasses.UnityDatabase
{
    /// <summary>
    /// Describe database record functional
    /// that can be only in Unity Editor
    /// </summary>

    public interface IEditorRecord
    {
        /// <summary>
        /// Set record unique identifier
        /// </summary>
        void SetId(int id);
    }
}

#endif
