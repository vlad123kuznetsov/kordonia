﻿#if UNITY_EDITOR
using EJaw.UnityDatabase.Records;
using EJaw.UnityDatabase.Tables;
using UnityEditor;

namespace EJaw.EditorClasses.UnityDatabase.Windows
{
    /// <summary>
    /// Display table in the window with foldouts
    /// </summary>
    public abstract class FoldoutsTableWindow<TTable, TRecord> : BaseTableWindow<TTable, TRecord>
        where TTable : ScriptableObjectTable, ITable<TRecord>, IEditorTable<TRecord>
        where TRecord : IRecord, IEditorRecord, new()
    {
        private bool selectTable = false;
        private bool tableData = false;
        private bool recordsData = false;

        protected override void DispayTableData()
        {
            tableData = EditorGUILayout.Foldout(tableData, "Table data");
            if (!tableData) return;
            base.DispayTableData();
        }

        protected override void DisplayRecordsData()
        {
            recordsData = EditorGUILayout.Foldout(recordsData, "Records");
            if (!recordsData) return;
            base.DisplayRecordsData();
        }

        protected override void SelectTable()
        {
            selectTable = EditorGUILayout.Foldout(selectTable, "Choose table");
            if (!selectTable) return;
            base.SelectTable();
        }
    }
}

#endif
