﻿using System;
using EJaw.UnityDatabase.Records;
using UnityEngine;

namespace EJaw.UnityDatabase
{
    [Serializable]
    public abstract class ServerIdRecord : BaseRecord
    {
        [SerializeField] protected string serverId;

        public string ServerId
        {
            get { return serverId; }
            set { serverId = value; }
        }

    }
}