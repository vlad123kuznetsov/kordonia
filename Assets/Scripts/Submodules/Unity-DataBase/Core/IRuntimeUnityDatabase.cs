﻿
using EJaw.UnityDatabase.Records;
using EJaw.UnityDatabase.Tables;

namespace EJaw.UnityDatabase
{
    /// <summary>
    /// Unity database functional that avaliable in runtime
    /// </summary>
    public interface IRuntimeUnityDatabase
    {
        /// <summary>
        /// Return table by record type
        /// </summary>
        ITable<TRecord> GetTable<TRecord>() where TRecord : IRecord;

        /// <summary>
        /// Return record by its id
        /// </summary>
        TRecord GetRecord<TRecord>(int recordId) where TRecord : IRecord;

        /// <summary>
        /// Return table by type
        /// </summary>
        TTable GetTableDirectly<TTable>() where TTable : ScriptableObjectTable;
    }
}
