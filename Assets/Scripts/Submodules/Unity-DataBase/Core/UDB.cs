﻿using EJaw.UnityDatabase.Tables;
using UnityEngine;
using System.Linq;

namespace EJaw.UnityDatabase
{
#if UNITY_EDITOR
    using EditorClasses.UnityDatabase.Helpers;
    using System.IO;
    using UnityEditor;
#endif

    /// <summary>
    /// Main class for unity database
    /// </summary>
    public static class UDB
    {
        #region Runtime database

        private static IRuntimeUnityDatabase runtimeUnityDatabase = null;

        public static Sprite GetSprite<TRecord>(string internalId) where TRecord : ServerRecordWithSprite
        {
            return RuntimeInstance.GetTable<TRecord>().Records.First(p => p.ServerId == internalId).Sprite;
        }

        public static IRuntimeUnityDatabase RuntimeInstance
        {
            get
            {
                if (runtimeUnityDatabase == null)
                {
                    var td = LoadTablesDatabase();
#if UNITY_EDITOR
                    if (td == null)
                        td = CreateTablesDatabase();
#endif
                    runtimeUnityDatabase = new RuntimeUnityDatabase(td);
                }

                return runtimeUnityDatabase;
            }
        }

        #endregion

        #region Functions

        private static TablesDatabase LoadTablesDatabase()
        {
            return Resources.Load("TablesDatabase") as TablesDatabase;
        }


#if UNITY_EDITOR

        private static TablesDatabase tablesDatabase = null;

        public static TablesDatabase GetTablesDatabase()
        {
            if (tablesDatabase == null)
                tablesDatabase = CreateTablesDatabase();

            return tablesDatabase;
        }

        private static TablesDatabase CreateTablesDatabase()
        {
            var td = LoadTablesDatabase();
            if (td != null)
                return td;

            if (!Directory.Exists("Assets/Resources"))
                AssetDatabase.CreateFolder("Assets", "Resources");
            td = ScriptableObjectHelper.Create<TablesDatabase>("Assets/Resources/TablesDatabase.asset");

            if (td != null)
                Debug.Log("UnityDatabase: tables database created!");
            else
                Debug.LogError("UnityDatabase: failed create tables database!");

            return td;
        }

        public static TTable Create<TTable>(string tableName) where TTable : ScriptableObjectTable
        {
            if (!Directory.Exists("Assets/Tables"))
                AssetDatabase.CreateFolder("Assets", "Tables");

            return ScriptableObjectHelper.Create<TTable>("Assets/Tables/" + tableName + ".asset");
        }

#endif

        #endregion
    }
}