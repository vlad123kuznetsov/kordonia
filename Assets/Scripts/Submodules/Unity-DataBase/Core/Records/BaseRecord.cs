﻿using System;
using UnityEngine;

namespace EJaw.UnityDatabase.Records
{
#if UNITY_EDITOR
    using EditorClasses.UnityDatabase;
#endif

    /// <summary>
    /// Base implementation of database record.
    /// You can inherit for creation you own records.
    /// </summary>
    [Serializable]
    public abstract class BaseRecord : IRecord
#if UNITY_EDITOR
        , IEditorRecord
#endif
    {
        /// <summary>
        /// Unique identifier in table 
        /// </summary>
        [SerializeField] private int id = IdGenerator.INVALID_ID;

        public int Id
        {
            get { return id; }
        }

#if UNITY_EDITOR
        void IEditorRecord.SetId(int id)
        {
            this.id = id;
        }
#endif
    }
}

