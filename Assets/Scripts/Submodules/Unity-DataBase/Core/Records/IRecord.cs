﻿
namespace EJaw.UnityDatabase.Records
{
    /// <summary>
    /// Describe database record functional
    /// that can be used in runtime and in editor
    /// </summary>
    public interface IRecord
    {
        /// <summary>
        /// Return record unique identifier in table 
        /// </summary>
        int Id { get; }
    }
}