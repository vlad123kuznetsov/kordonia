﻿using EJaw.UnityDatabase.Records;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace EJaw.UnityDatabase.Tables
{
#if UNITY_EDITOR
    using EditorClasses.UnityDatabase;
    using EditorClasses.UnityDatabase.Layouts;
#endif

    /// <summary>
    /// Base table functional 
    /// </summary>
    public abstract class BaseTable<TRecord> : ScriptableObjectTable, ITable<TRecord>
#if UNITY_EDITOR
        , IEditorTable<TRecord>
#endif
        where TRecord : class, IRecord
#if UNITY_EDITOR
            , IEditorRecord
#endif
    {
        #region Fields

        /// <summary>
        /// Need for serialization of records
        /// </summary>
        [SerializeField] private List<TRecord> records = new List<TRecord>();

        #endregion Fields

        #region ITable

        public TRecord GetRecord(int id)
        {
            return records.FirstOrDefault(r => r.Id == id);
        }

        public TRecord NextRecord(int id)
        {
            var r = GetRecord(id);
            if (r == null) return null;
            var index = records.IndexOf(r);
            if (index < 0) return null;
            index++;
            return records.ElementAtOrDefault(index);
        }

        public IEnumerable<TRecord> Records
        {
            get { return records; }
        }

        #endregion

        #region IEditorTable

#if UNITY_EDITOR

        void IEditorTable<TRecord>.Add(TRecord record)
        {
            if (!records.Contains(record))
            {
                var ids = records.Select(r => r.Id);

                record.SetId(IdGenerator.GetUniqueIdSmart(ids));
                records.Add(record);
                Debug.Log(string.Format("BaseTable: record successfully added to {0} table.", name));
            }
            else
                Debug.LogWarning(string.Format("BaseTable: can't add record to {0} table due to it duplication!", name));
        }

        void IEditorTable<TRecord>.Add(TRecord record, TRecord prevRecord)
        {
            var ids = records.Select(r => r.Id);

            record.SetId(IdGenerator.GetUniqueIdSmart(ids));
            records.Insert(records.IndexOf(prevRecord), record);
        }

        void IEditorTable<TRecord>.Remove(int id)
        {
            Remove(id);
        }

        void IEditorTable<TRecord>.Remove(TRecord record)
        {
            if (record != null)
                Remove(record.Id);
        }

        bool IEditorTable<TRecord>.Contains(int id)
        {
            var record = records.FirstOrDefault(r => r.Id == id);
            if (record != null) return true;

            return false;
        }

        void IEditorTable<TRecord>.TableHeaders(ITableLayout layout)
        {
            TableHeaders(layout);
        }

        void IEditorTable<TRecord>.TableData(ITableLayout layout)
        {
            TableData(layout);
        }

        void IEditorTable<TRecord>.RecordsHeaders(ITableLayout layout)
        {
            RecordsHeaders(layout);
        }

        void IEditorTable<TRecord>.RecordData(ITableLayout layout, TRecord record)
        {
            RecordData(layout, record);
        }

        void IEditorTable<TRecord>.SwapRecordUp(TRecord record)
        {

            var indexOfRecord = records.IndexOf(record);

            if (indexOfRecord <= 0)
                return;

            var tempRecord = records[indexOfRecord - 1];
            records[indexOfRecord - 1] = records[indexOfRecord];
            records[indexOfRecord] = tempRecord;


        }

        void IEditorTable<TRecord>.SwapRecordDown(TRecord record)
        {
            var indexOfRecord = records.IndexOf(record);
            if (indexOfRecord >= records.Count - 1)
                return;

            var tempRecord = records[indexOfRecord + 1];
            records[indexOfRecord + 1] = records[indexOfRecord];
            records[indexOfRecord] = tempRecord;
        }

        void IEditorTable<TRecord>.Sort(Comparison<TRecord> comparison)
        {
            Sort(comparison);
        }

#endif

        #endregion

        #region Functions

        /// <summary>
        /// Return table's recod type
        /// </summary>
        public override Type RecordType
        {
            get { return typeof (TRecord); }
        }
        
#if UNITY_EDITOR

        protected virtual void TableHeaders(ITableLayout layout)
        {
        }

        protected virtual void TableData(ITableLayout layout)
        {
        }

        protected virtual void RecordsHeaders(ITableLayout layout)
        {
        }

        protected virtual void RecordData(ITableLayout layout, TRecord record)
        {
        }

        protected void Sort(Comparison<TRecord> comparison)
        {
            records.Sort(comparison);
        }

        private void Remove(int id)
        {
            var record = records.FirstOrDefault(r => r.Id == id);

            if (record != null)
            {
                records.Remove(record);
                Debug.Log(string.Format("BaseTable: record successfully removed from {0} table.", name));
            }
            else
            {
                Debug.LogWarning(
                    string.Format("BaseTable: can't remove record with id {0} from {1} because it didn't exist!",
                        id,
                        name));
            }
        }


#endif

        #endregion

    }
}
