﻿using EJaw.UnityDatabase.Records;
using System.Collections.Generic;

namespace EJaw.UnityDatabase.Tables
{
    /// <summary>
    /// Describe database table functional
    /// that can be used in runtime
    /// </summary>
    public interface ITable<TRecord> where TRecord : IRecord
    {
        /// <summary>
        /// Return record from table
        /// by its id
        /// </summary>
        TRecord GetRecord(int id);

        /// <summary>
        /// Return next record after
        /// record with <param name="id"></param>
        /// </summary>
        TRecord NextRecord(int id);

        /// <summary>
        /// Return all records that table contains
        /// </summary>
        IEnumerable<TRecord> Records { get; }
    }
}
