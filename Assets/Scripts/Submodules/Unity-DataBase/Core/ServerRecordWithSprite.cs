﻿using System;
using UnityEngine;

namespace EJaw.UnityDatabase
{
    [Serializable]
    public abstract class ServerRecordWithSprite : ServerIdRecord
    {
        [SerializeField] protected Sprite sprite;

        public Sprite Sprite
        {
            get { return sprite; }
            set { sprite = value; }
        }
    }
}