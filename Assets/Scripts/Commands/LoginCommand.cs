﻿using Assets.Scripts.Services;
using Kordonia.Data;
using strange.extensions.command.impl;

namespace Assets.Scripts.Commands
{
    public class LoginCommand : EventCommand
    {
        [Inject]
        public IUserService UserService { get; set; }

        public override void Execute()
        {
            var fakeName = string.Format("Player {0}", UnityEngine.Random.Range(0, 1000));
            Retain();
            UserService.LoginOrSignUp(new LoginInfo() {Name = fakeName}, b =>
            {
                Release();
            });
        }
    }
}