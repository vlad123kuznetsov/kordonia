﻿using Assets.Scripts.Services;
using Kordonia.Data;
using strange.extensions.command.impl;

namespace Assets.Scripts.Commands
{
    public class JoinRoomCommand : EventCommand
    {
        [Inject]
        public UserProfile Profile { get; set; }

        public override void Execute()
        {
            var levelInitializer = Container.Get<ILevelInitializer>();
            levelInitializer.ConnectWithName(Profile.Name, dispatcher);
        }
    }
}