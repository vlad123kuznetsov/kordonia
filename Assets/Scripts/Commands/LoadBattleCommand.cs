﻿using strange.extensions.command.impl;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Commands
{
    public class LoadBattleCommand : EventCommand
    {
        public override void Execute()
        {
            SceneManager.LoadScene(Scenes.Battle.ToString());
        }
    }
}