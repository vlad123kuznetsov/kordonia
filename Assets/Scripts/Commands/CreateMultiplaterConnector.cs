﻿using Assets.Scripts.Services;
using strange.extensions.command.impl;
using UnityEngine;

namespace Assets.Scripts.Commands
{
    public class CreateMultiplaterConnector : EventCommand
    {
        public override void Execute()
        {
            var multiplayerConnector = new GameObject {name = "Connector"};
            var connector = multiplayerConnector.AddComponent<RoomConnector>();
            multiplayerConnector.AddComponent<DontDestroyOnLoad>();
            Container.Add(typeof(ILevelInitializer), connector);

        }
    }
}