﻿using UnityEngine;

namespace Assets.Scripts
{
    public class ViewWithTarget<T> : MonoBehaviour
    {
        public T Model
        {
            get; protected set;
        }

        public virtual void Create(T model)
        {
            Model = model;
        }
    }
}