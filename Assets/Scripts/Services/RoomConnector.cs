﻿using System.Linq;
using Assets.Scripts.Commands;
using Assets.Scripts.Data;
using Photon;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace Assets.Scripts.Services
{
    public class RoomConnector : PunBehaviour, ILevelInitializer
    {
        private IEventDispatcher dispatcher;
        private const string serverVersion = "0.5";

        private void Awake()
        {
            if (PhotonNetwork.connected || PhotonNetwork.connecting)
            {
                PhotonNetwork.Disconnect();;
            }
        }

        public void ConnectWithName(string name, IEventDispatcher _dispatcher)
        {
            dispatcher = _dispatcher;

            if (PhotonNetwork.AuthValues == null)
            {
                PhotonNetwork.AuthValues = new AuthenticationValues();
            }

            PhotonNetwork.playerName = name;
            PhotonNetwork.autoJoinLobby = true;
            PhotonNetwork.ConnectUsingSettings(serverVersion);

            dispatcher.Dispatch(GameEvents.ShowView, new ShowViewParams()
            {
                isAdditive = false,
                viewName = ViewType.WaitForOpponent
            });
        }

        public override void OnConnectedToMaster()
        {
            var rooms = PhotonNetwork.GetRoomList();
            var openedRoom = rooms.FirstOrDefault(p => p.IsOpen && p.PlayerCount == 1);
            if (openedRoom != null)
            {
                PhotonNetwork.JoinRoom(openedRoom.Name);
            }
            else
            {
                PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 2, PlayerTtl = 5000 }, null);
            }
        }

        public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            if (PhotonNetwork.playerList.Length == 2)
            {
                dispatcher.Dispatch(GameEvents.StartPlaningPhase);
            }
        }

        public override void OnJoinedLobby()
        {
            OnConnectedToMaster(); 
        }

        public override void OnDisconnectedFromPhoton()
        {
            dispatcher.Dispatch(GameEvents.ShowView, new ShowViewParams()
            {
                isAdditive = false,
                viewName = ViewType.Disconnect
            });
        }

        public override void OnConnectionFail(DisconnectCause cause)
        {
            dispatcher.Dispatch(GameEvents.ShowView, new ShowViewParams()
            {
                isAdditive = false,
                viewName = ViewType.Disconnect
            });
        }
    }
}