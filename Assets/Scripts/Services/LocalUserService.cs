﻿using System;
using System.Linq;
using EJaw.UnityDatabase;
using Kordonia.Data;
using UnityEngine;

namespace Assets.Scripts.Services
{
    public class LocalUserService : IUserService
    {
        [Inject] public UserProfile UserProfile { get; set; }

        private const int InitialLevel = 1;
        private const int InitialRating = 1500;

        public void LoginOrSignUp(object loginData, Action<bool> onComplete)
        {
            var info = loginData as LoginInfo;
            var needSave = false;
            var allWarriors = UDB.RuntimeInstance.GetTable<WarriorDescription>().Records;

            SerializableUser serializableUser = null;

            if (PlayerPrefs.HasKey(PrefsKeys.User))
            {
                serializableUser = JsonUtility.FromJson<SerializableUser>(PlayerPrefs.GetString(PrefsKeys.User));
            }
            else
            {
                needSave = true;
                serializableUser = new SerializableUser()
                {
                    Name = info.Name,
                    aviableWarriors = allWarriors.Select(p => p.Name).ToList(),
                    Id = Guid.NewGuid().ToString(),
                    Level = InitialLevel,
                    Rating = InitialRating
                };
            }

            UserProfile.Id = serializableUser.Id;
            UserProfile.Rating = serializableUser.Rating;
            UserProfile.AviableWarriors = allWarriors.Where(p => serializableUser.aviableWarriors.Contains(p.Name));
            UserProfile.Name = serializableUser.Name;

            if (needSave)
            {
                SaveUser(onComplete);
            }
            else
            {
                if (onComplete != null)
                {
                    onComplete(true);
                }
            }
        }

        public void SaveUser(Action<bool> onComplete)
        {
            var serializableUser = new SerializableUser()
            {
                Id = UserProfile.Id,
                aviableWarriors = UserProfile.AviableWarriors.Select(p => p.Name).ToList(),
                Level = UserProfile.Level,
                Name = UserProfile.Name,
                Rating = UserProfile.Rating
            };

            PlayerPrefs.SetString(PrefsKeys.User, JsonUtility.ToJson(serializableUser));
        }
    }
}