﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class Container
    {
        private static IDictionary<Type, object> services = new Dictionary<Type, object>();

        public static void Add(Type type, object o)
        {
            if (services.ContainsKey(type))
            {
                services[type] = o;
            }
            else
            {
                services.Add(type, o);
            }
        }

        public static T Get<T>() where T : class 
        {
            if (services.ContainsKey(typeof(T)))
            {
                return services[typeof(T)] as T;
            }

            Debug.LogError("not found service of type " + typeof(T).ToString());
            return null;
        }
    }
}