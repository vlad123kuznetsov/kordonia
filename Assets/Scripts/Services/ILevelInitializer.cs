﻿using strange.extensions.dispatcher.eventdispatcher.api;

namespace Assets.Scripts.Services
{
    public interface ILevelInitializer
    {
        void ConnectWithName(string name, IEventDispatcher _dispatcher);
    }
}