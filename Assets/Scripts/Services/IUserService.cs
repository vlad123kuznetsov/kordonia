﻿using System;

namespace Assets.Scripts.Services
{
    public interface IUserService
    {
        void LoginOrSignUp(object loginData, Action<bool> onComplete);
        void SaveUser(Action<bool> onComplete);
    }
}