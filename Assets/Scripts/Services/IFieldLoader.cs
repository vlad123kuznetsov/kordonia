﻿using System.Collections.Generic;
using Kordonia.Data;

namespace Assets.Scripts.Services
{
    public interface  IFieldLoader
    {
        CellViewModel[,] Load(string lvlId);
    }
}