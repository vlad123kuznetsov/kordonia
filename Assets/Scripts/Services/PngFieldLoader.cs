﻿using System.Collections.Generic;
using Kordonia.Data;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts.Services
{
    public class PngFieldLoader : IFieldLoader
    {
        private Color forest = Color.black; 

        public CellViewModel[,] Load(string lvlId)
        {
            var res = Resources.Load<Texture2D>(string.Format("levels/{0}", lvlId));
            Assert.IsNotNull(res, "Can't load level with id "  + lvlId);

            var textureData = res.GetPixels(0, 0, res.width, res.height);
            var result = new CellViewModel[res.width,res.height];

            for (int j = 0; j < res.height; j++)
            {
                for (int i = 0; i < res.width; i++)
                {
                    var newCell = new CellViewModel
                    {
                        col = i,
                        row = j,
                        type = textureData[j*res.height + i] == forest
                            ? CellType.Forest
                            : CellType.Simple,
                        isInitial = j == 0 || j == res.height - 1
                    };

                    result[i, j] = newCell;
                }
            }

            return result;
        }
    }
}