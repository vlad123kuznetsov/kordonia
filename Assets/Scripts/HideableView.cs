﻿using Assets.Scripts.Data;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.Scripts
{
    public abstract class HideableView : EventView
    {
        [SerializeField] private GameObject objectToHide;
        [SerializeField] private ViewType viewType;

        public ViewType ViewType
        {
            get { return viewType; }
        }

        public bool IsActive
        {
            get { return objectToHide.activeInHierarchy; }
        }

        public void Show()
        {
            objectToHide.SetActive(true);
        }

        public void Hide()
        {
            objectToHide.SetActive(false);
        }
    }
}