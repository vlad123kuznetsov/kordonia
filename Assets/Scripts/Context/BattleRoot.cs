﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.context.impl;

public class BattleRoot : ContextView
{
    private void Awake()
    {
        context = new BattleContext(this);
    }
}