﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts;
using Assets.Scripts.Commands;
using Assets.Scripts.Data;
using Kordonia.View;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

public class BattleContext : MVCSContext
{
    public BattleContext(MonoBehaviour view) : base(view)
    {
    }

    public BattleContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
    {
    }

    protected override void mapBindings()
    {
        commandBinder.Bind(GameEvents.FindOpponent).To<JoinRoomCommand>();

        mediationBinder.Bind<HidebleViewsManager>().To<HideableViewMediator>();
        mediationBinder.Bind<SelectWarriorView>().To<SelectWarriorMediator>();
    }
}