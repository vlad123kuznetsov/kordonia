﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum BattleStates
{
    SelectWarriors,
    WaitForOpponentSelection,
    PlayerTurn,
    GameStarted,
    GameFinished
}