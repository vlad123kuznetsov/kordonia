﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EJaw.UnityDatabase.Records;
using UnityEngine;

namespace Kordonia.Data
{
    public class WarriorDescription : BaseRecord
    {
        public Sprite Sprite;
        public string Name;
        public string Description;
        public int Price;
        public WarriorType Type;
    }

    public enum WarriorType
    {
        Human
    }
}

