﻿namespace Assets.Scripts.Data
{
    public enum GameEvents
    {
        ShowView,
        StartPlaningPhase,
        FindOpponent,
        LoadField
    }
}