﻿namespace Assets.Scripts.Data
{
    public class ShowViewParams
    {
        public ViewType viewName;
        public bool isAdditive;
    }
}