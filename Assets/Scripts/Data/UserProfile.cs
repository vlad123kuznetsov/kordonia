﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace Kordonia.Data
{
    public sealed class UserProfile
    {
        public string Name;
        public string Id;
        public int Level;
        public int Rating;
        public IEnumerable<WarriorDescription> AviableWarriors;
    }

    [Serializable]
    public sealed class SerializableUser
    {
        public string Name;
        public string Id;
        public int Level;
        public int Rating;
        public List<string> aviableWarriors;
    }


}
