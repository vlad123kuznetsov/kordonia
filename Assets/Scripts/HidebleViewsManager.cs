﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Data;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    public class HidebleViewsManager : EventView
    {
        [SerializeField] private List<HideableView> views = new List<HideableView>();

        public void Show(object param)
        {
            var casted = param as ShowViewParams;
            Assert.IsNotNull(casted, "Invalid params for show view");
            var requestedView = views.Find(p => p.ViewType == casted.viewName);
            Assert.IsNotNull(requestedView, "Missing view with type " + casted.viewName);

            requestedView.Show();

            if (!casted.isAdditive)
            {
                views.Where(p => p.ViewType != requestedView.ViewType).ToList().ForEach(p => p.Hide());
            }
        }
    }
}