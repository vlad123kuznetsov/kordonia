﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts;
using Kordonia.Data;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class CellViewInfo
{
    public Sprite sprite;
    public CellType type;
}

public class CellView : ViewWithTarget<CellViewModel>
{
    [SerializeField] private Color planingColor = Color.yellow;
    [SerializeField] private Color selectedColor = Color.blue;

    [SerializeField] private List<CellViewInfo> cellInfo = new List<CellViewInfo>();
    [SerializeField] private Image img;
    [SerializeField] private Button btn;

    private bool _selectable = false;
    private bool _isInitial = false;

    public float HalfHeight
    {
        get
        {
            var texture = cellInfo.First().sprite.texture;
            return texture.height / 2f;
        }
    }

    public bool Selectable
    {
        get { return _selectable; }
        set
        {
            _selectable = value;
            if (!value || Model.type == CellType.Forest)
            {
                img.color = Color.white;
            }
            else
            {
                img.color = selectedColor;
            }
        }
    }

    public bool Initial
    {
        get { return _isInitial; }
        set
        {
            _isInitial = value;
            img.color = value ? planingColor : Color.white;
        }
    }

    public override void Create(CellViewModel model)
    {
        img.sprite = cellInfo.First(p => p.type == Model.type).sprite;
        img.color = Color.white;
    }
}