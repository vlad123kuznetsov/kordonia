﻿using System.Linq;
using System.Text;
using Assets.Scripts;
using Kordonia.Data;
using UnityEngine;
using UnityEngine.UI;

namespace Kordonia.View
{
    public class SelectWarriorViewItem : ViewWithTarget<WarriorDescription>
    {
        [SerializeField] private Image img;
        [SerializeField] private Text price;
        [SerializeField] private Button btn;

        private bool _inEquipment;

        public Button Button
        {
            get { return btn; }
        }

        public override void Create(WarriorDescription model)
        {
            base.Create(model);
            img.sprite = Model.Sprite;
            price.text = Model.Price + "";
        }

        public bool InEquipment
        {
            get { return _inEquipment; }
            set
            {
                _inEquipment = value;
                price.gameObject.SetActive(value);
            }
        }
    }
}