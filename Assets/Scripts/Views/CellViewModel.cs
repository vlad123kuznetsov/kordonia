﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kordonia.Data
{
    public class CellViewModel
    {
        public CellType type;
        public bool isInitial;
        public int row;
        public int col;
    }
}