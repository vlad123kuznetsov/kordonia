﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts;
using Assets.Scripts.Data;
using Kordonia.Data;
using Kordonia.View;
using strange.extensions.mediation.impl;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Kordonia.View
{
    public class SelectWarriorView : HideableView
    {
        [SerializeField] private RectTransform selectedView;
        [SerializeField] private RectTransform aviableView;

        [SerializeField] private Button confirmSelection;
        [SerializeField] private Text aviablePricePoints;
        [SerializeField] private SelectWarriorViewItem itemPrefab;

        [Inject]
        private UserProfile Profile { get; set; }

        private List<WarriorDescription> inEquipment = new List<WarriorDescription>();
        private const int totalPricePoints = 20;

        private void OnEnable()
        {
            foreach (var warriorItem in Profile.AviableWarriors)
            {
                var newInstanse = Instantiate(itemPrefab);
                newInstanse.transform.SetParent(aviableView);
                newInstanse.transform.localScale = Vector3.one;
                newInstanse.Create(warriorItem);
                newInstanse.Button.OnClickAsObservable().RepeatUntilDisable(this.gameObject).Subscribe(p =>
                {
                    if (newInstanse.InEquipment)
                    {
                        newInstanse.InEquipment = false;
                        newInstanse.transform.SetParent(aviableView);
                        inEquipment.Remove(newInstanse.Model);
                        confirmSelection.interactable = inEquipment.Any();
                    }
                    else
                    {
                        var aviablePoints = totalPricePoints - EquipmentPrice;
                        if (newInstanse.Model.Price <= aviablePoints)
                        {
                            newInstanse.InEquipment = true;
                            inEquipment.Add(newInstanse.Model);
                            newInstanse.transform.SetParent(aviableView);
                        }
                    }

                    aviablePricePoints.text = (totalPricePoints - EquipmentPrice).ToString();
                });

                confirmSelection.OnClickAsObservable().RepeatUntilDisable(this.gameObject).Subscribe(p =>
                {
                    Hide();
                    dispatcher.Dispatch(GameEvents.FindOpponent);
                });

                aviablePricePoints.text = totalPricePoints.ToString();

            }
        }

        private int EquipmentPrice
        {
            get { return inEquipment.Sum(p => p.Price); }
        }
    }

    public class SelectWarriorMediator : EventMediator { }
}

