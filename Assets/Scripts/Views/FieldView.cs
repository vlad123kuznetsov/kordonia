﻿using System;
using System.Linq;
using System.Text;
using Assets.Scripts.Data;
using Assets.Scripts.Services;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.Assertions;

public class FieldView : EventView
{
    [SerializeField] private CellView cellView;
    [SerializeField] private GameObject rightBottomCorner;

    [Inject] public IFieldLoader loader { get; set; }

    private CellView[,] cellViews;

    public void LoadField(object o)
    {
        var fieldName = o as string;
        Assert.IsNotNull(fieldName, "Invalid load level param type " + o.GetType());
        var cellData = loader.Load(fieldName);
        cellViews = new CellView[cellData.GetLength(0), cellData.GetLength(1)];

        for (int i = 0; i < cellData.GetLength(0); i++)
        {
            for (int j = 0; j < cellData.GetLength(1); j++)
            {
                var cellInstance = Instantiate(cellView);
                cellInstance.transform.SetParent(rightBottomCorner.transform);
                cellInstance.transform.localScale = Vector3.one;
                var delta = j%2 == 1 ? cellInstance.HalfHeight : 0;
                cellInstance.transform.localPosition = new Vector3(cellInstance.HalfHeight * 2 * i, j * 2 * cellInstance.HalfHeight + delta);
                cellViews[i, j] = cellInstance;
                cellInstance.Create(cellData[i, j]);
            }
        }
    }
}



public class FieldMediator : EventMediator
{
    [Inject] public FieldView view { get; set; }

    public override void OnRegister()
    {
        dispatcher.AddListener(GameEvents.LoadField, view.LoadField);
    }

    public override void OnRemove()
    {
        dispatcher.RemoveListener(GameEvents.LoadField, view.LoadField);
    }
}